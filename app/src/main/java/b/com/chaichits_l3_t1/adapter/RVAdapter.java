package b.com.chaichits_l3_t1.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import b.com.chaichits_l3_t1.FifthActivity;
import b.com.chaichits_l3_t1.R;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.StoryViewHolder> {

    private List<Story> stories;

    public void setupData(List<Story> stories) {
        this.stories = stories;
        notifyDataSetChanged();
    }

    @Override
    public StoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card, viewGroup, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), FifthActivity.class);
                v.getContext().startActivity(intent);
            }
        });
        return new StoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(StoryViewHolder storyViewHolder, int i) {
        storyViewHolder.title.setText(stories.get(i).title);
        storyViewHolder.text.setText(stories.get(i).text);
    }

    @Override
    public int getItemCount() {
        return stories.size();
    }

    public static class StoryViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView text;

        StoryViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tvTitle);
            text = (TextView) itemView.findViewById(R.id.tvText);
        }
    }
}