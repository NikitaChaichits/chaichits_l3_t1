package b.com.chaichits_l3_t1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import b.com.chaichits_l3_t1.adapter.RVAdapter;
import b.com.chaichits_l3_t1.adapter.Story;

import static b.com.chaichits_l3_t1.SecondActivity.APP_PREFERENCES;

public class ForthActivity extends AppCompatActivity {

    List<Story> stories = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forth_layout);

        setInitialData ();

        RecyclerView recyclerView = findViewById(R.id.rv);
        RVAdapter adapter = new RVAdapter();
        recyclerView.setAdapter(adapter);

        adapter.setupData(stories);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ForthActivity.this);
                builder.setTitle("Alert!")
                        .setMessage("You will logout. Are you sure?")
                        .setCancelable(false)
                        .setPositiveButton("YES",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        startActivity(new Intent(ForthActivity.this, SecondActivity.class));
                                        SharedPreferences sp = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor e = sp.edit();
                                        e.clear();
                                        e.apply();
                                    }
                                })
                        .setNegativeButton("No, please no!",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    private void setInitialData() {
        for (int i = 1; i < 10; i++) {
            stories.add(new Story(getResources().getString(R.string.title_4th),
                    getResources().getString(R.string.text_4th)));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ForthActivity.this, MainActivity.class));
    }
}
