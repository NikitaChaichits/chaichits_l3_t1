package b.com.chaichits_l3_t1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity{
    public static final String APP_PREFERENCES = "mysettings";

    public CheckBox mCheckBox;
    public SharedPreferences sp;
    private EditText email, pass;
    private Button btnSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_layout);
        sp = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        boolean hasVisited = sp.getBoolean("hasLogged", false);
        if (hasVisited) {
            Intent intent = new Intent(SecondActivity.this, ForthActivity.class);
            startActivity(intent);
        }

        email = findViewById(R.id.etEmail2nd);
        pass = findViewById(R.id.etPass2nd);
        mCheckBox = findViewById(R.id.checkBox2nd);
        btnSignIn = findViewById(R.id.btn_signin_2nd);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!email.getText().toString().equals("")
                        & !pass.getText().toString().equals("")
                        & mCheckBox.isChecked()) {
                    SharedPreferences.Editor e = sp.edit();
                    e.putBoolean("hasLogged", true);
                    e.apply();
                    Intent intent = new Intent(SecondActivity.this, ForthActivity.class);
                    startActivity(intent);
                }else if (!email.getText().toString().equals("")
                        & !pass.getText().toString().equals("")) {
                    Intent intent = new Intent(SecondActivity.this, ForthActivity.class);
                    startActivity(intent);
                }else {
                    Toast.makeText(v.getContext(), "Enter email and password please", Toast.LENGTH_SHORT).show();                }

            }
        });
    }
}
